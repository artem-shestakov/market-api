package model

import (
	"encoding/json"
	"net/http"
)

type Product struct {
	ID          int     `json:"id"`
	CategoryID  int     `json:"category_id" db:"category_id"`
	Title       string  `json:"title" validate:"required"`
	Description string  `json:"description"`
	Price       float32 `json:"price" validate:"required"`
	Currency    string  `json:"currency" validate:"required"`
}

func (p *Product) FromJSON(r *http.Request) error {
	decoder := json.NewDecoder(r.Body)
	return decoder.Decode(p)
}

type Products struct {
	Products []Product `json:"products"`
}

type UpdateProduct struct {
	CategoryID  int     `json:"category_id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Price       float32 `json:"price"`
	Currency    string  `json:"currency"`
}

func (up *UpdateProduct) FromJSON(r *http.Request) error {
	decoder := json.NewDecoder(r.Body)
	return decoder.Decode(up)
}

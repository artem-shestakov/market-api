package model

type HttpError struct {
	ErrorCode int
	Message   string
}

func (e *HttpError) Code() int {
	return e.ErrorCode
}

func (e *HttpError) Error() string {
	return e.Message
}

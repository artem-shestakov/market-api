package model

import (
	"encoding/json"
	"io"
)

type Category struct {
	ID          int    `json:"id"`
	Title       string `json:"title" validate:"required"`
	Description string `json:"description"`
}

func (c *Category) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(c)
}

type Categories struct {
	Categories []Category `json:"categories"`
}

type UpdateCategory struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

func (c *UpdateCategory) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(c)
}

type CategoryResponse struct {
	ID       int       `json:"category_id"`
	Total    int       `json:"total_items"`
	Products []Product `json:"products"`
}

package model

import (
	"encoding/json"
	"io"
)

type User struct {
	ID        int    `json:"id" db:"id"`
	FirstName string `json:"f_name"`
	LastName  string `json:"l_name"`
	Email     string `json:"email" validate:"required,email"`
	Password  string `json:"password" validate:"required"`
	Role      string `json:"role"`
	Active    bool   `json:"active"`
}

func (u *User) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(u)
}

type SignInInput struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (s *SignInInput) FromJSON(r io.Reader) error {
	decoder := json.NewDecoder(r)
	return decoder.Decode(s)
}

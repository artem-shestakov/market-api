package model

import (
	"encoding/json"
	"net/http"
)

// Cart object
type Cart struct {
	ID        int    `json:"id"`
	UserID    int    `json:"user_id" db:"user_id"`
	Status    string `json:"status"`
	CreatedAT string `json:"created_at" db:"created_at"`
	UpdatedAt string `json:"updated_at" db:"updated_at"`
}

// Convert JSON request body to object Cart
func (c *Cart) FronJSON(r http.Request) error {
	decoder := json.NewDecoder(r.Body)
	return decoder.Decode(c)
}

type Carts struct {
	Carts []Cart `json:"carts"`
}

type CartRow struct {
	ProductID    int     `json:"id" db:"id"`
	ProductTitle string  `json:"product_title" db:"title"`
	Price        float32 `json:"price" db:"price"`
	Quantity     int     `json:"qty" db:"quantity"`
	Sum          float32 `json:"sum" db:"sum"`
}

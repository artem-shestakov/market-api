package service

import (
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
	"gitlab.com/artem-shestakov/market-api/internal/app/repository"
)

type AdminService struct {
	repository repository.Admin
}

func NewAdminService(repository repository.Admin) *AdminService {
	return &AdminService{
		repository: repository,
	}
}

func (s *AdminService) ListAllCarts() ([]model.Cart, error) {
	return s.repository.ListAllCarts()
}

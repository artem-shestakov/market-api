package service

import (
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
	"gitlab.com/artem-shestakov/market-api/internal/app/repository"
)

type CartService struct {
	repository repository.Cart
}

func NewCartService(repository repository.Cart) *CartService {
	return &CartService{
		repository: repository,
	}
}

func (s *CartService) CreateCart(user_id int) (int, error) {
	return s.repository.CreateCart(user_id)
}

func (s *CartService) GetNewCart(user_id int) (int, error) {
	return s.repository.GetNewCart(user_id)
}

func (s *CartService) GetUserCart(user_id int) ([]model.CartRow, error) {
	return s.repository.GetUserCart(user_id)
}

package service

import (
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
	"gitlab.com/artem-shestakov/market-api/internal/app/repository"
)

type CategoryService struct {
	repository repository.Category
}

func NewCategoryService(repository repository.Category) *CategoryService {
	return &CategoryService{
		repository: repository,
	}
}

func (s *CategoryService) CreateCategory(category *model.Category) (int, error) {
	return s.repository.CreateCategory(category)
}

func (s *CategoryService) GetAllCategory() ([]model.Category, error) {
	return s.repository.GetAllCategory()
}

func (s *CategoryService) GetAllCategoryProducts(categoryID int) ([]model.Product, error) {
	return s.repository.GetAllCategoryProducts(categoryID)
}

func (s *CategoryService) GetByID(id int) (model.Category, error) {
	return s.repository.GetByID(id)
}

func (s *CategoryService) DeleteByID(id int) error {
	return s.repository.DeleteByID(id)
}

func (s *CategoryService) Update(id int, data *model.UpdateCategory) error {
	return s.repository.Update(id, data)
}

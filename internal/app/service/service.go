package service

import (
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
	"gitlab.com/artem-shestakov/market-api/internal/app/repository"
)

type Admin interface {
	ListAllCarts() ([]model.Cart, error)
}

type Authorization interface {
	CreateUser(user *model.User, role string) (int, error)
	GenerateToken(email, password string) (string, error)
	ParserToken(accessToken string) (int, error)
	GetUserRole(id int) (string, error)
}

type Category interface {
	CreateCategory(category *model.Category) (int, error)
	GetAllCategory() ([]model.Category, error)
	GetAllCategoryProducts(categoryID int) ([]model.Product, error)
	GetByID(id int) (model.Category, error)
	DeleteByID(id int) error
	Update(id int, data *model.UpdateCategory) error
}

type Product interface {
	CreateProduct(categoryID int, product *model.Product) (int, error)
	GetAllProducts() ([]model.Product, error)
	GetProduct(id int) (model.Product, error)
	DeleteProduct(id int) error
	Update(id int, data *model.UpdateProduct) (*model.Product, error)
	AddToCard(product_id int, cart_id int, quantity int) error
}

type Cart interface {
	CreateCart(user_id int) (int, error)
	GetNewCart(user_id int) (int, error)
	GetUserCart(user_id int) ([]model.CartRow, error)
}

type Service struct {
	Admin
	Authorization
	Category
	Product
	Cart
}

func NewService(repository *repository.Repository) *Service {
	return &Service{
		Admin:         NewAdminService(repository.Admin),
		Authorization: NewAuthService(repository.Authorization),
		Category:      NewCategoryService(repository.Category),
		Product:       NewProductService(repository.Product),
		Cart:          NewCartService(repository.Cart),
	}
}

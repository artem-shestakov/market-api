package service

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
	"gitlab.com/artem-shestakov/market-api/internal/app/repository"
)

const (
	salt       = "jdS7a8nfGanMD71f77Gpqdg61"
	signingKey = "a8dfMw1amC0w4aNL7hlL4p3lk2KdlT2qasd"
	tokenTTL   = 12 * time.Hour
)

type tokenClaims struct {
	jwt.StandardClaims
	UserID int
}

type AuthService struct {
	repository repository.Authorization
}

func NewAuthService(repository repository.Authorization) *AuthService {
	return &AuthService{
		repository: repository,
	}
}

func (s *AuthService) CreateUser(user *model.User, role string) (int, error) {
	user.Password = hashPassword(user.Password)
	user.Role = role
	user.Active = true
	return s.repository.CreateUser(user)
}

func (s *AuthService) GetUserRole(id int) (string, error) {
	return s.repository.GetUserRole(id)
}

func (s *AuthService) GenerateToken(email, password string) (string, error) {
	user, err := s.repository.GetUser(email, hashPassword(password))
	if err != nil {
		return "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(tokenTTL).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
		user.ID,
	})

	return token.SignedString([]byte(signingKey))
}

func (s *AuthService) ParserToken(accessToken string) (int, error) {
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(signingKey), nil
	})
	if err != nil {
		return 0, err
	}
	if !token.Valid {
		return 0, errors.New("Token is not valid")
	}
	if claims, ok := token.Claims.(*tokenClaims); !ok {
		return 0, errors.New("Token claims are not of type \"tokenClaims\"")
	} else {
		return claims.UserID, nil
	}
}

func hashPassword(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))
	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}

package service

import (
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
	"gitlab.com/artem-shestakov/market-api/internal/app/repository"
)

type ProductService struct {
	repository repository.Product
}

func NewProductService(repository repository.Product) *ProductService {
	return &ProductService{
		repository: repository,
	}
}

func (s *ProductService) CreateProduct(categoryID int, product *model.Product) (int, error) {
	return s.repository.CreateProduct(categoryID, product)
}

func (s *ProductService) GetAllProducts() ([]model.Product, error) {
	return s.repository.GetAllProducts()
}

func (s *ProductService) GetProduct(id int) (model.Product, error) {
	return s.repository.GetProduct(id)
}

func (s *ProductService) DeleteProduct(id int) error {
	return s.repository.DeleteProduct(id)
}

func (s *ProductService) Update(id int, data *model.UpdateProduct) (*model.Product, error) {
	return s.repository.Update(id, data)
}

func (s *ProductService) AddToCard(product_id int, cart_id int, quantity int) error {
	return s.repository.AddToCard(product_id, cart_id, quantity)
}

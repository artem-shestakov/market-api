package handler

import (
	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"gitlab.com/artem-shestakov/market-api/internal/app/service"
)

type Handler struct {
	service   *service.Service
	validator *validator.Validate
}

func NewHandler(service *service.Service, validator *validator.Validate) *Handler {
	return &Handler{
		service:   service,
		validator: validator,
	}
}

func (h *Handler) InitRouter() *mux.Router {
	router := mux.NewRouter()

	// Healthcheck route
	router.Handle("/ping", h.Ping()).Methods("GET")

	// Auth route
	authRoute := router.PathPrefix("/auth").Subrouter()
	authRoute.Handle("/sign-up", h.SignUp()).Methods("POST")
	authRoute.Handle("/sign-in", h.SignIn()).Methods("POST")

	// API endpoints
	apiV1Route := router.PathPrefix("/api/v1").Subrouter()
	apiV1Route.Use(h.AuthMiddleware)

	categoryV1Route := apiV1Route.PathPrefix("/category").Subrouter()
	categoryV1Route.Handle("", h.GetAllCategory()).Methods("GET")
	categoryV1Route.Handle("", h.CreateCategory()).Methods("POST")
	categoryV1Route.Handle("/{id:[0-9]+}", h.GetCategoryByID()).Methods("GET")
	categoryV1Route.Handle("/{id:[0-9]+}/products", h.GetAllCategoryProducts()).Methods("GET")
	categoryV1Route.Handle("/{id:[0-9]+}", h.DeleteCategory()).Methods("DELETE")
	categoryV1Route.Handle("/{id:[0-9]+}", h.UpdateCategory()).Methods("PUT")

	productsV1Route := apiV1Route.PathPrefix("/product").Subrouter()
	productsV1Route.Handle("", h.GetAllProducts()).Methods("GET")
	productsV1Route.Handle("/{id:[0-9]+}", h.GetProduct()).Methods("GET")
	productsV1Route.Handle("", h.CreateProduct()).Methods("POST")
	productsV1Route.Handle("/{id:[0-9]+}", h.DeleteProduct()).Methods("DELETE")
	productsV1Route.Handle("/{id:[0-9]+}", h.UpdateProduct()).Methods("PUT")
	productsV1Route.Handle("/{id:[0-9]+}/cart", h.AddToCard()).Methods("POST")

	cartV1Route := apiV1Route.PathPrefix("/cart").Subrouter()
	cartV1Route.Handle("", h.CreateCart()).Methods("POST")
	cartV1Route.Handle("", h.GetUserCart()).Methods("GET")

	adminV1Route := apiV1Route.PathPrefix("/admin").Subrouter()
	adminV1Route.Handle("/carts", h.ListAllCarts()).Methods("GET")

	return router
}

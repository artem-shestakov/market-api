package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

/**
* @api {post} /auth/sign-up Sign up new user
* @apiName SignUp
* @apiGroup Auth
* @apiversion 0.0.1
*
* @apiExample {curl} Example usage:
* curl --request POST 'http://127.0.0.1:8080/auth/sign-up' \
--header 'Content-Type: application/json' \
--data-raw '{
    "f_name": "Artem",
    "l_name": "Shestakov",
    "email": "artem.s.shestakov@yandex.ru",
    "password": "password"
}'
*
* @apiParam (Body) {String} f_name User's first name
* @apiParam (Body) {String} l_name User's last name
* @apiParam (Body) {String} email User's e-mail address
* @apiParam (Body) {String} password User's account password
*
* @apiParamExample {json} Example:
*	{
*    	"f_name": "Artem",
*    	"l_name": "Shestakov",
*    	"email": "artem.s.shestakov@yandex.ru",
*    	"password": "password"
*	}
*
* @apiSuccessExample {json} 200:
*	{
*		"user_id": 1
*   }
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) SignUp() http.HandlerFunc {
	user := model.User{}
	return func(rw http.ResponseWriter, r *http.Request) {
		if err := user.FromJSON(r.Body); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		if err := h.validator.Struct(user); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		fmt.Println(user)
		id, err := h.service.Authorization.CreateUser(&user, "user")
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"user_id": id,
		})
	}
}

/**
* @api {post} /auth/sign-in Sign into your account
* @apiName SignIn
* @apiGroup Auth
* @apiversion 0.0.1
*
* @apiExample {curl} Example usage:
* curl --request POST 'http://127.0.0.1:8080/auth/sign-in' \
* --header 'Content-Type: application/json' \
* --data-raw '{
*     "email": "artem.s.shestakov@yandex.ru",
*     "password": "password"
* }'
*
* @apiParam (Body) {String} email User's e-mail address
* @apiParam (Body) {String} password User's account password
*
* @apiParamExample {json} Example:
*	{
*    	"email": "artem.s.shestakov@yandex.ru",
*    	"password": "password"
*	}
*
* @apiSuccessExample {json} 200:
*	{
*		"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjU1OTc2ODEsImlhdCI6MTYyNTU1NDQ4MSwiVXNlcklEIjoyfQ.KmSqVbPZOeNh5j15bs39AjdfTOZnnZyqVOTtg7-AaG8"
*   }
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */
func (h *Handler) SignIn() http.HandlerFunc {
	signInInput := model.SignInInput{}
	return func(rw http.ResponseWriter, r *http.Request) {
		if err := signInInput.FromJSON(r.Body); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		if err := h.validator.Struct(signInInput); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		token, err := h.service.Authorization.GenerateToken(signInInput.Email, signInInput.Password)
		if err != nil {
			h.Error(rw, http.StatusUnauthorized, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"token": token,
		})
	}
}

package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

type ErrorResponse struct {
	Error       string `json:"error"`
	Description string `json:"description"`
}

func (h *Handler) Response(rw http.ResponseWriter, code int, message interface{}) {
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(code)
	jsonBytes, err := json.Marshal(message)
	if err != nil {
		code = http.StatusInternalServerError
		rw.WriteHeader(code)
		json.NewEncoder(rw).Encode(map[string]string{
			"error": fmt.Sprintf("%d - JSON marshaling error", code),
		})
	}
	rw.Write(jsonBytes)
}

/**
* @apiDefine ApiError_4xx
* @apiError (4xx) {String} error Error text
* @apiError (4xx) {String} description Error description
 */

/**
* @apiDefine ApiError_5xx
* @apiError (5xx) {String} error Error text
* @apiError (5xx) {String} description Error description
 */

/**
* @apiDefine Error_400
* @apiError (4xx) {String} error Error description
*
* @apiErrorExample {json} 400
*	{
*		"error": "The request cannot be fulfilled due to bad syntax",
*    	"description": "invalid character '\"' after object key:value pair"
*
*	}
 */

/**
* @apiDefine Error_401
* @apiErrorExample {json} 401
*	{
*		"error": "Request is unauthorized",
*		"description": "Authorization header is not found in request",
*	}
 */

/**
* @apiDefine Error_403
* @apiErrorExample {json} 403
*	{
*		"error": "Access forbidden",
*		"description": "User has no role \"administrator\"",
*	}
 */

/**
* @apiDefine Error_404
* @apiErrorExample {json} 404
*	{
*    	"description": "sql: no rows in result set",
*    	"error": "Not found"
*	}
 */

/**
* @apiDefine Error_405
* @apiErrorExample {sting} 405
*	nill
 */

/**

* @apiDefine Error_500

* @apiErrorExample {json} 500
*	{
*		"error": "Internal server error. Please contact administrator",
*    	"description": "dial tcp 127.0.0.1:5432: connect: connection refused"
*	}
 */

func (h *Handler) Error(rw http.ResponseWriter, code int, errDescr string) {
	message := ""
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(code)
	switch code {
	case 400:
		message = "The request cannot be fulfilled due to bad syntax"
	case 401:
		message = "Request is unauthorized"
	case 403:
		message = "Access forbidden"
	case 404:
		message = "Object is not found"
	case 500:
		message = "Internal server error. Please contact administrator"
	default:
		message = "Unknown error. Please contact administrator"
	}
	errResponse := ErrorResponse{
		Error:       message,
		Description: errDescr,
	}
	jsonBytes, err := json.Marshal(&errResponse)

	if err != nil {
		code = http.StatusInternalServerError
		rw.WriteHeader(code)
		json.NewEncoder(rw).Encode(map[string]string{
			"error":       "JSON marshaling error",
			"description": "Error with error handler",
		})
		return
	}
	rw.Write(jsonBytes)
}

func (h *Handler) CheckRole(ID int, requiredRole string) error {
	role, err := h.service.GetUserRole(ID)
	if err != nil {
		return err
	}
	if role != requiredRole {
		return errors.New("User has no role " + "\"" + requiredRole + "\"")
	}
	return nil
}

func (h *Handler) CheckRoleNew(ID int, requiredRole string) *model.HttpError {
	role, err := h.service.GetUserRole(ID)
	if err != nil {
		return &model.HttpError{ErrorCode: 500, Message: err.Error()}
	}
	if role != requiredRole {
		return &model.HttpError{ErrorCode: 403, Message: fmt.Sprintf("User has no role "+"\"%s\"", requiredRole)}
	}
	return nil
}

package handler

import (
	"net/http"

	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

/**
* @api {get} /api/v1/carts List all carts
* @apiName ListCarts
* @apiGroup Administration
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiPermission administrator
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8000/api/v1/carts' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjYzMTIzMzksImlhdCI6MTYyNjI2OTEzOSwiVXNlcklEIjoxfQ.iD5EM45b41EkQYHMdvWS22m8LC93rS2cKaYrOASMCFA'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiSuccess (201) {String} carts List of all carts
*
* @apiSuccessExample {json} 201:
*	{
    "carts": [
        {
            "id": 1,
            "user_id": 1,
            "status": "new",
            "created_at": "2021-07-14T13:25:53.991161Z",
            "updated_at": "2021-07-14T13:25:53.991161Z"
        },
        {
            "id": 2,
            "user_id": 2,
            "status": "shopping",
            "created_at": "2021-07-14T13:47:53.631313Z",
            "updated_at": "2021-07-14T13:47:53.631313Z"
        }
    ]
}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_403
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) ListAllCarts() http.HandlerFunc {
	carts := model.Carts{}
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user's ID from token
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Check user's role. If not administrator stop request
		err = h.CheckRole(userID, "administrator")
		if err != nil {
			h.Response(rw, http.StatusForbidden, err.Error())
			return
		}
		carts.Carts, err = h.service.ListAllCarts()
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, carts)
	}
}

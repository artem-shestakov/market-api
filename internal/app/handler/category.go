package handler

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

/**
* @api {post} /api/v1/category Create new category
* @apiName CreateCategory
* @apiGroup Category
* @apiversion 0.0.1
*
* @apiPermission administrator
* @apiExample {curl} Example usage:
* curl --location --request POST 'http://127.0.0.1:8080/api/v1/category' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjUyNTcxNDUsImlhdCI6MTYyNTIxMzk0NSwiVXNlcklEIjoxfQ.qSohyLon0a-NASbfClOL-LbeDlieYjHiYELd7f9zWE8' \
--header 'Content-Type: application/json' \
--data-raw \
'{
   "title": "books",
   "description": "The collections of books and e-books"
}'
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiParam (Body) {String} titte Category title
* @apiParam (Body) {String} [description] Category description
*
* @apiParamExample {json} Example:
*	{
*    	"title": "books",
*    	"description": "The collections of books and e-books",
*	}
*
* @apiSuccess (201) {String} user_id Created category's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"category_id": "1"
*	}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_403
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) CreateCategory() http.HandlerFunc {
	category := model.Category{}
	return func(rw http.ResponseWriter, r *http.Request) {
		// Check user's role
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		if err = h.CheckRole(userID, "administrator"); err != nil {
			if err.Error() == "Can't pind database" {
				h.Error(rw, http.StatusInternalServerError, err.Error())
				return
			}
			h.Error(rw, http.StatusForbidden, err.Error())
			return
		}
		// Unmarshal JSON
		if err := category.FromJSON(r.Body); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Check data
		if err := h.validator.Struct(category); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Create row
		id, err := h.service.CreateCategory(&category)
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"category_id": id,
		})
	}
}

/**
* @api {get} /api/v1/category List all categories
* @apiName GetAllCategory
* @apiGroup Category
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8080/api/v1/category' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjUyNTcxNDUsImlhdCI6MTYyNTIxMzk0NSwiVXNlcklEIjoxfQ.qSohyLon0a-NASbfClOL-LbeDlieYjHiYELd7f9zWE8'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiSuccess (201) {String[]} categories List of categories
*
* @apiSuccessExample {json} 201:
*	{
    "categories": [
        {
            "id": 1,
            "title": "moto",
            "description": "Great motorcycles"
        },
        {
            "id": 3,
            "title": "books",
            "description": "The collections of books and e-books"
        }
    ]
}
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) GetAllCategory() http.HandlerFunc {
	allCategories := new(model.Categories)
	return func(rw http.ResponseWriter, r *http.Request) {
		categories, err := h.service.Category.GetAllCategory()
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		allCategories.Categories = categories
		h.Response(rw, http.StatusOK, allCategories)
	}
}

/**
* @api {get} /api/v1/category/:title/products List products of category
* @apiName GetCategoryProducts
* @apiGroup Category
* @apiversion 0.0.1
*
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8080/api/v1/category/book/products' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjU3ODg1ODUsImlhdCI6MTYyNTc0NTM4NSwiVXNlcklEIjoxfQ.Qc0dYDZ9Y-9qqtIe0cmCEMgpASB8afAuqirHy70tT40'
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam (URL параметр) {String} title Category title.
*
* @apiSuccess (200) {String} category Category title
* @apiSuccess (200) {String} products The products of category
*
* @apiSuccessExample {json} 200:
*	{
*	"category": "books"
*   "products": [
*		{
*           "id": 1,
*           "category_id": 1,
*           "title": "First book",
*           "description": "Your first book",
*           "price": 1000
*		},
*       {
*           "id": 2,
*           "category_id": 1,
*           "title": "Second book",
*           "description": "",
*           "price": 999
*       }
*   ]
* }
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_404
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */
func (h *Handler) GetAllCategoryProducts() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get category ID from request
		vars := mux.Vars(r)
		categoryID, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
		}
		// Get products
		products, err := h.service.Category.GetAllCategoryProducts(categoryID)
		if err != nil {
			if strings.Contains(err.Error(), "Can't found category") {
				h.Error(rw, http.StatusNotFound, err.Error())
				return
			}
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		// allProducts.Products = products
		categoryResponse := model.CategoryResponse{
			ID:       categoryID,
			Total:    len(products),
			Products: products,
		}
		h.Response(rw, http.StatusOK, categoryResponse)
	}
}

/**
* @api {get} /api/v1/category/:id Get category by ID
* @apiName GetCategoryByID
* @apiGroup Category
* @apiversion 0.0.1
*
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8080/api/v1/category/3' --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjUyNTcxNDUsImlhdCI6MTYyNTIxMzk0NSwiVXNlcklEIjoxfQ.qSohyLon0a-NASbfClOL-LbeDlieYjHiYELd7f9zWE8'
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam (URL параметр) {Number} id Category ID.
*
* @apiSuccess (200) {Number} id ID of category
* @apiSuccess (200) {String} title Category name
* @apiSuccess (200) {String} description information about category
*
* @apiSuccessExample {json} 200:
*	{
*       "id": 3,
*       "title": "books",
*       "description": "The collections of books and e-books"
*   }
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_404
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */

func (h *Handler) GetCategoryByID() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get id from request
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Get row by id
		category, err := h.service.Category.GetByID(id)
		if err != nil {
			if strings.Contains(err.Error(), "connection refused") {
				h.Error(rw, http.StatusInternalServerError, err.Error())
				return
			}
			h.Error(rw, http.StatusNotFound, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, category)
	}
}

/**
* @api {delete} /api/v1/category/:id Delete category by ID
* @apiName DeleteCategoryByID
* @apiGroup Category
* @apiversion 0.0.1
*
* @apiExample {curl} Example usage:
* curl --request DELETE 'http://127.0.0.1:8080/api/v1/category/3' --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjUyNTcxNDUsImlhdCI6MTYyNTIxMzk0NSwiVXNlcklEIjoxfQ.qSohyLon0a-NASbfClOL-LbeDlieYjHiYELd7f9zWE8'
*
* @apiPermission administrator
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam (URL параметр) {Number} id Category ID.
*
* @apiSuccessExample {json} 200:
*	{
		null
*   }
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_403
* @apiUse Error_404
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) DeleteCategory() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Check user's role
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		if err = h.CheckRole(userID, "administrator"); err != nil {
			h.Error(rw, http.StatusForbidden, err.Error())
			return
		}
		// Get id from request
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Delete row
		err = h.service.Category.DeleteByID(id)
		if err != nil {
			if err.Error() == "sql: no rows in result set" {
				h.Error(rw, http.StatusNotFound, err.Error())
				return
			}
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, nil)
	}
}

/**
* @api {put} /api/v1/category/:id Update category by ID
* @apiName UpdateCategoryByID
* @apiGroup Category
* @apiversion 0.0.1
*
* @apiExample {curl} Example usage:
* curl --request PUT 'http://127.0.0.1:8080/api/v1/category/3' --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjUyNTcxNDUsImlhdCI6MTYyNTIxMzk0NSwiVXNlcklEIjoxfQ.qSohyLon0a-NASbfClOL-LbeDlieYjHiYELd7f9zWE8' \
--header 'Content-Type: application/json' \
--data-raw \
'{
    "title": "foo",
    "description": "bar"
}'
*
* @apiPermission administrator
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiParam (URL параметр) {Number} id Category ID.
*
* @apiParam (Body) {String} [titte] Category title
* @apiParam (Body) {String} [description] Category description
*
* @apiParamExample {json} Example:
*	{
*    	"title": "books",
*    	"description": "The collections of books and e-books",
*	}
*
* @apiSuccessExample {json} 200:
*	{
		updated_category_id": 3
*   }
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_403
* @apiUse Error_404
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) UpdateCategory() http.HandlerFunc {
	data := new(model.UpdateCategory)
	return func(rw http.ResponseWriter, r *http.Request) {
		// Check user's role
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		if err = h.CheckRole(userID, "administrator"); err != nil {
			h.Error(rw, http.StatusForbidden, err.Error())
			return
		}
		// Get id from equest
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		if err := data.FromJSON(r.Body); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Update row
		err = h.service.Category.Update(id, data)
		if err != nil {
			if err.Error() == "sql: no rows in result set" {
				h.Error(rw, http.StatusNotFound, err.Error())
				return
			}
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"updated_category_id": id,
		})
	}
}

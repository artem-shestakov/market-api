package handler

import (
	"net/http"
)

/**
* @api {post} /api/v1/cart Create new cart
* @apiName CreateCart
* @apiGroup Cart
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiExample {curl} Example usage:
* curl --request POST 'http://127.0.0.1:8000/api/v1/cart' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjYzMTIzMzksImlhdCI6MTYyNjI2OTEzOSwiVXNlcklEIjoxfQ.iD5EM45b41EkQYHMdvWS22m8LC93rS2cKaYrOASMCFA'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiSuccess (201) {String} user_id Created category's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"cart_id": 1
*	}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */
func (h *Handler) CreateCart() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user's ID from request
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		cartID, err := h.service.CreateCart(userID)
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"cart_id": cartID,
		})
	}
}

/**
* @api {post} /api/v1/cart List product in cart
* @apiName ListProductCart
* @apiGroup Cart
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8000/api/v1/cart' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjYzMTIzMzksImlhdCI6MTYyNjI2OTEzOSwiVXNlcklEIjoxfQ.iD5EM45b41EkQYHMdvWS22m8LC93rS2cKaYrOASMCFA'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiSuccess (201) {String} user_id Created category's ID
*
* @apiSuccessExample {json} 201:
*	{
    "products": [
        {
            "id": 2,
            "product_title": "Изучаем Python. Программирование игр, визуализация данных, веб-приложения Мэтиз Эрик",
            "price": 1250,
            "qty": 2,
            "sum": 2500
        },
        {
            "id": 6,
            "product_title": "Язык программирования Go | Донован Алан А. А., Керниган Брайан У.",
            "price": 1650,
            "qty": 1,
            "sum": 1650
        }
    ]
}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) GetUserCart() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user's ID from request
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		products, err := h.service.Cart.GetUserCart(userID)
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"products": products,
		})
	}
}

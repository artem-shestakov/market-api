package handler

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

/**
* @api {post} /api/v1/product?category={category_id} Create new product
* @apiName CreateProduct
* @apiGroup Product
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiPermission administrator
* @apiExample {curl} Example usage:
* curl --request POST 'http://127.0.0.1:8080/api/v1/product?category=books' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjU2MTAxNTEsImlhdCI6MTYyNTU2Njk1MSwiVXNlcklEIjoxfQ.QfuzOw1sG64fAcGb_gDQ3s6jOvXpSGbXEIyggjrBayk' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "Awesome book",
    "description": "Realy awesome book",
    "price": 1000
}'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiParam (URL) {String} category_id Product's category
*
* @apiParam (Body) {String} title New product title
* @apiParam (Body) {String} [description] The description of product
* @apiParam (Body) {String} [description] Product's price
*
* @apiParamExample {json} Example:
*	{
*    	"title": "Awesome book",
*	    "description": "Realy awesome book",
*	    "price": 1000
*	}
*
* @apiSuccess (201) {String} user_id Created category's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"product_id": 1
*	}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_403
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) CreateProduct() http.HandlerFunc {
	product := new(model.Product)
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user's ID from token
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Check user's role. If not administrator stop request
		err = h.CheckRole(userID, "administrator")
		if err != nil {
			h.Response(rw, http.StatusForbidden, err.Error())
			return
		}
		// Get category ID from request param
		params := r.URL.Query()
		if len(params) < 1 {
			h.Error(rw, http.StatusBadRequest, "No params in request")
			return
		}
		categoryID, err := strconv.Atoi(params["category"][0])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Parse JSON from request body
		if err := product.FromJSON(r); err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Check reguest body structure
		if err := h.validator.Struct(product); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Create product
		product_id, err := h.service.Product.CreateProduct(categoryID, product)
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		// Return new product's ID
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"product_id": product_id,
		})
	}
}

/**
* @api {get} /api/v1/product List all product
* @apiName ListProducts
* @apiGroup Product
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8080/api/v1/product' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjU3ODg1ODUsImlhdCI6MTYyNTc0NTM4NSwiVXNlcklEIjoxfQ.Qc0dYDZ9Y-9qqtIe0cmCEMgpASB8afAuqirHy70tT40'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiSuccess (201) {String} products List of products
*
* @apiSuccessExample {json} 201:
*	{
    "products": [
		{
            "id": 1,
            "category_id": 1,
            "title": "First book",
            "description": "Your first book",
            "price": 1000
		},
        {
            "id": 2,
            "category_id": 1,
            "title": "Second book",
            "description": "",
            "price": 999
        }
    ]
}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
*/
func (h *Handler) GetAllProducts() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		allProducts := new(model.Products)
		products, err := h.service.Product.GetAllProducts()
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		allProducts.Products = products
		h.Response(rw, http.StatusOK, allProducts)
	}
}

/**
* @api {post} /api/v1/product/:id Get product by ID
* @apiName GetProduct
* @apiGroup Product
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8080/api/v1/product/2' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjU3ODg1ODUsImlhdCI6MTYyNTc0NTM4NSwiVXNlcklEIjoxfQ.Qc0dYDZ9Y-9qqtIe0cmCEMgpASB8afAuqirHy70tT40'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam (URL) {Number} id Product's ID
*
* @apiSuccess (201) {String} user_id Created category's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"id": 1,
*       "category_id": 1,
*       "title": "First book",
*       "description": "Your first book",
*       "price": 1000
*	}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_404
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */
func (h *Handler) GetProduct() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get product ID from URL
		vars := mux.Vars(r)
		productID, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Get product by ID
		product, err := h.service.Product.GetProduct(productID)
		if err != nil {
			if strings.Contains(err.Error(), "no rows in result set") {
				h.Error(rw, http.StatusNotFound, err.Error())
				return
			}
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, product)
	}
}

/**
* @api {delete} /api/v1/product/:id Delete product by ID
* @apiName DeleteProduct
* @apiGroup Product
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiExample {curl} Example usage:
* curl --request DELETE 'http://127.0.0.1:8080/api/v1/product/3' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjYxMjQyMjMsImlhdCI6MTYyNjA4MTAyMywiVXNlcklEIjoxfQ.ePInPqjwIY7qDOkgHs8gOI4reGIs_dGXTDQuZin57DU'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo"
*   }
*
* @apiParam (URL) {Number} id Product's ID
*
* @apiSuccess (201) {String} null
*
* @apiSuccessExample {string} 201:
*	nill
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_403
* @apiUse Error_404
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */
func (h *Handler) DeleteProduct() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user's ID from token
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Check user's role. If not administrator stop request
		err = h.CheckRole(userID, "administrator")
		if err != nil {
			h.Response(rw, http.StatusForbidden, err.Error())
			return
		}
		// Get product ID from URL
		vars := mux.Vars(r)
		productID, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		err = h.service.Product.DeleteProduct(productID)
		if err != nil {
			if strings.Contains(err.Error(), "Can't found product ID") {
				h.Error(rw, http.StatusNotFound, err.Error())
				return
			}
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, nil)
	}
}

/**
* @api {put} /api/v1/product/:id Update product by ID
* @apiName UpdateProduct
* @apiGroup Product
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
*
* @apiExample {curl} Example usage:
* curl --request PUT 'http://127.0.0.1:8080/api/v1/product/7' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjYxMjQyMjMsImlhdCI6MTYyNjA4MTAyMywiVXNlcklEIjoxfQ.ePInPqjwIY7qDOkgHs8gOI4reGIs_dGXTDQuZin57DU' \
* --header 'Content-Type: application/json' \
* --data-raw '{
*     "price": 999
* }
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiParam (URL) {Number} id Product's ID
*
* @apiSuccess (201) {Object} product Updated product
*
* @apiSuccessExample {string} 201:
*	{
*     "id": 20,
*     "category_id": 1,
*     "title": "Golang для профи. Работа с сетью, многопоточность, структуры данных и машинное обучение с Go Цукалос Михалис | Цукалос Михалис",
*     "description": "Вам уже знакомы основы языка Go? В таком случае эта книга для вас. Михалис Цукалос продемонстрирует возможности языка, даст понятные и простые объяснения, приведет примеры и предложит эффективные паттерны программирования. Изучая нюансы Go, вы освоите типы и структуры данных языка, а также работу с пакетами, конкурентность, сетевое программирование, устройство компиляторов, оптимизацию и многое другое. Закрепить новые знания помогут материалы и упражнения в конце каждой главы. Уникальным материалом станет глава о машинном обучении на языке Go, в которой вы пройдёте от основополагающих статистических приемов до регрессии и кластеризации. Вы изучите классификацию, нейронные сети и приёмы выявления аномалий. Из прикладных разделов вы узнаете: как использовать Go с Docker и Kubernetes, Git, WebAssembly, JSON и др.",
*     "price": 3260,
*     "currency": "ruble"
*	}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_403
* @apiUse Error_404
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */
func (h *Handler) UpdateProduct() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user's ID from token
		newData := model.UpdateProduct{}
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Check user's role. If not administrator stop request
		appErr := h.CheckRoleNew(userID, "administrator")
		if appErr != nil {
			h.Error(rw, appErr.Code(), err.Error())
			return
		}
		// Get product ID from URL
		vars := mux.Vars(r)
		productID, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Get data from request
		if err := newData.FromJSON(r); err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		product, err := h.service.Product.Update(productID, &newData)
		if err != nil {
			if strings.Contains(err.Error(), "Can't found product ID") {
				h.Error(rw, http.StatusNotFound, err.Error())
				return
			}
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		h.Response(rw, http.StatusOK, product)
	}
}

/**
* @api {post} /api/v1/product/:id/cart?quantity={Number} Add to cart
* @apiName AddCart
* @apiGroup Product
* @apiversion 0.0.1
*
* @apiHeader {String} Authorization Bearer authorization token.
* @apiExample {curl} Example usage:
* curl --request POST 'http://127.0.0.1:8000/api/v1/product/7/cart?quantity=2' \
* --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjYzMTIzMzksImlhdCI6MTYyNjI2OTEzOSwiVXNlcklEIjoxfQ.iD5EM45b41EkQYHMdvWS22m8LC93rS2cKaYrOASMCFA'
*
* @apiHeaderExample {json} Header-Example:
*   {
*       "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjI2Njg0NzcsImlhdCI6MTYyMjYyNTI3NywidXNlcl9pZCI6MTB9.0aOR7SJHE9ycZa0ihPR6Q7rICCjKPauUbEiUJw16aPo",
*   }
*
* @apiParam (URL) {Number} product_id Product's ID
* @apiParam (URL) {Number} quantity Quantity of product
*
* @apiSuccess (201) {String} user_id Created category's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"product_id": 7,
*    	"quantity": 2,
*	}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_400
* @apiUse Error_401
* @apiUse Error_405
*
* @apiUse ApiError_5xx
* @apiUse Error_500
 */
func (h *Handler) AddToCard() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// get user's ID from request
		userID, err := h.GetUserID(rw, r)
		if err != nil {
			h.Response(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Get product ID from URL
		vars := mux.Vars(r)
		productID, err := strconv.Atoi(vars["id"])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Get quantity param
		params := r.URL.Query()
		if len(params) < 1 {
			h.Error(rw, http.StatusBadRequest, "No params in request")
			return
		}
		quantity, err := strconv.Atoi(params["quantity"][0])
		if err != nil {
			h.Error(rw, http.StatusBadRequest, err.Error())
			return
		}
		// Check exists user's carts
		cartID, err := h.service.Cart.GetNewCart(userID)
		// In not exists new cart
		if cartID == 0 {
			// Create new cart
			cartID, err = h.service.Cart.CreateCart(userID)
		}
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}
		// Add product to cart
		err = h.service.Product.AddToCard(productID, cartID, quantity)
		if err != nil {
			h.Error(rw, http.StatusInternalServerError, err.Error())
			return
		}

		h.Response(rw, http.StatusOK, map[string]interface{}{
			"product_id": productID,
			"quantity":   quantity,
		})
	}
}

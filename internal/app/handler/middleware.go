package handler

import (
	"context"
	"errors"
	"net/http"
	"strings"
)

func (h *Handler) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			h.Error(rw, http.StatusUnauthorized, "Authorization header is not found in request")
			return
		}
		bearerToken := strings.Split(authHeader, " ")
		if len(bearerToken) != 2 {
			h.Error(rw, http.StatusUnauthorized, "Check authorization Bearer token format")
			return
		}
		userID, err := h.service.ParserToken(bearerToken[1])
		if err != nil {
			h.Error(rw, http.StatusUnauthorized, err.Error())
			return
		}

		ctx := context.WithValue(r.Context(), "userID", userID)
		next.ServeHTTP(rw, r.WithContext(ctx))
	})
}

func (h *Handler) GetUserID(rw http.ResponseWriter, r *http.Request) (int, error) {
	userID := r.Context().Value("userID")
	if userID == "" {
		return 0, errors.New("User ID if not found in request")
	}
	id, ok := userID.(int)
	if !ok {
		return 0, errors.New("User ID is of invalid type")
	}
	return id, nil
}

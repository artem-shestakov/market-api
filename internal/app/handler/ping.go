package handler

import "net/http"

/**
* @api {get} /ping App health check
* @apiName HealthCheck
* @apiGroup Health Check
* @apiversion 0.0.1
*
* @apiExample {curl} Example usage:
* curl --request GET 'http://127.0.0.1:8080/ping'
*
* @apiSuccess (201) {String} user_id Created category's ID
*
* @apiSuccessExample {json} 201:
*	{
*   	"ping": "pong"
*	}
*
* @apiUse ApiError_4xx
*
* @apiUse Error_405
 */
func (h *Handler) Ping() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		h.Response(rw, http.StatusOK, map[string]interface{}{
			"ping": "pong",
		})
	}
}

package server

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

type Server struct {
	httpServer *http.Server
	logger     *logrus.Logger
}

func NewServer() *Server {
	return &Server{}
}

func (s *Server) Run(addr, port string, handler http.Handler) error {
	s.httpServer = &http.Server{
		Addr:    addr + ":" + port,
		Handler: handler,
	}
	logrus.Infof("Starting server on http://%s", s.httpServer.Addr)
	return s.httpServer.ListenAndServe()
}

package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

type AdminDB struct {
	db *sqlx.DB
}

func NewAdminDB(db *sqlx.DB) *AdminDB {
	return &AdminDB{
		db: db,
	}
}

func (r *AdminDB) ListAllCarts() ([]model.Cart, error) {
	var carts []model.Cart
	query := fmt.Sprintf("SELECT * FROM %s", cartsTable)
	err := r.db.Select(&carts, query)
	return carts, err
}

package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

const (
	usersTable         = "users"
	categoryTable      = "category"
	productsTable      = "products"
	cartsTable         = "carts"
	cartsProductsTable = "carts_products"
)

type Database struct {
	Address  string
	Port     string
	User     string
	Password string
	DBName   string
	SSLMode  string
}

func NewDB(db *Database) (*sqlx.DB, error) {
	database, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		db.Address, db.Port, db.User, db.DBName, db.Password, db.SSLMode))
	if err != nil {
		logrus.Errorf("can't open connection to database: %s:%s", db.Address, db.Port)
		logrus.Debug(err)
		return nil, err
	}
	err = database.Ping()
	if err != nil {
		logrus.Errorf("can't ping database: %s:%s", db.Address, db.Port)
		logrus.Debug(err)
		return nil, err
	}
	logrus.Infof("connected to database: %s:%s", db.Address, db.Port)
	logrus.Debug(err)
	return database, nil
}

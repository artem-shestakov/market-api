package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

type Authorization interface {
	CreateUser(user *model.User) (int, error)
	GetUser(email, password string) (model.User, error)
	GetUserRole(id int) (string, error)
}

type Category interface {
	CreateCategory(category *model.Category) (int, error)
	GetAllCategory() ([]model.Category, error)
	GetAllCategoryProducts(categoryID int) ([]model.Product, error)
	GetByID(id int) (model.Category, error)
	DeleteByID(id int) error
	Update(id int, data *model.UpdateCategory) error
}

type Product interface {
	CreateProduct(categoryID int, product *model.Product) (int, error)
	GetAllProducts() ([]model.Product, error)
	GetProduct(id int) (model.Product, error)
	DeleteProduct(id int) error
	Update(id int, data *model.UpdateProduct) (*model.Product, error)
	AddToCard(product_id int, cart_id int, quantity int) error
}

type Cart interface {
	CreateCart(user_id int) (int, error)
	GetNewCart(user_id int) (int, error)
	GetUserCart(user_id int) ([]model.CartRow, error)
}

type Admin interface {
	ListAllCarts() ([]model.Cart, error)
}

type Repository struct {
	Admin
	Authorization
	Category
	Product
	Cart
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Admin:         NewAdminDB(db),
		Authorization: NewAuthDB(db),
		Category:      NewCategoryDB(db),
		Product:       NewProductDB(db),
		Cart:          NewCardDB(db),
	}
}

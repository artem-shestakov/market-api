package repository

import (
	"errors"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

type CategoryDB struct {
	db *sqlx.DB
}

func NewCategoryDB(db *sqlx.DB) *CategoryDB {
	return &CategoryDB{
		db: db,
	}
}

func (r *CategoryDB) CreateCategory(category *model.Category) (int, error) {
	var categoryID int
	query := fmt.Sprintf("INSERT INTO %s (title, description) VALUES ($1, $2) RETURNING id", categoryTable)
	row := r.db.QueryRow(query, category.Title, category.Description)
	if err := row.Scan(&categoryID); err != nil {
		return 0, err
	}
	return categoryID, nil
}

func (r *CategoryDB) GetAllCategory() ([]model.Category, error) {
	var categories []model.Category
	query := fmt.Sprintf("SELECT * FROM %s", categoryTable)
	err := r.db.Select(&categories, query)
	return categories, err
}

func (r *CategoryDB) GetAllCategoryProducts(categoryID int) ([]model.Product, error) {
	var exists bool
	var products []model.Product
	// Check category exists
	categoryQuery := fmt.Sprintf("SELECT EXISTS (SELECT title FROM %s WHERE id=$1)", categoryTable)
	rowCategory := r.db.QueryRow(categoryQuery, categoryID)
	if err := rowCategory.Scan(&exists); err != nil || !exists {
		return nil, errors.New(fmt.Sprintf("Can't found category ID: %d", categoryID))
	}
	// Get products by category
	query := fmt.Sprintf("SELECT * FROM %s WHERE category_id=$1", productsTable)
	err := r.db.Select(&products, query, categoryID)
	return products, err
}

func (r *CategoryDB) GetByID(id int) (model.Category, error) {
	var category model.Category
	query := fmt.Sprintf("SELECT * FROM %s WHERE id=$1", categoryTable)
	err := r.db.Get(&category, query, id)
	return category, err
}

func (r *CategoryDB) DeleteByID(id int) error {
	_, err := r.GetByID(id)
	if err != nil {
		return err
	}
	query := fmt.Sprintf("DELETE FROM %s WHERE id=$1", categoryTable)
	_, err = r.db.Exec(query, id)
	return err
}

func (r *CategoryDB) Update(id int, data *model.UpdateCategory) error {
	_, err := r.GetByID(id)
	if err != nil {
		return err
	}
	newValues := make([]string, 0)
	args := make([]interface{}, 0)
	argID := 1

	if data.Title != "" {
		newValues = append(newValues, fmt.Sprintf("title=$%d", argID))
		args = append(args, data.Title)
		argID++
	}
	if data.Description != "" {
		newValues = append(newValues, fmt.Sprintf("description=$%d", argID))
		args = append(args, data.Description)
		argID++
	}

	setQuery := strings.Join(newValues, ", ")
	args = append(args, id)
	query := fmt.Sprintf("UPDATE %s SET %s WHERE id=$%d", categoryTable, setQuery, argID)
	_, err = r.db.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}

package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

type AuthDB struct {
	db *sqlx.DB
}

func NewAuthDB(db *sqlx.DB) *AuthDB {
	return &AuthDB{
		db: db,
	}
}

func (r *AuthDB) CreateUser(user *model.User) (int, error) {
	var id int
	query := fmt.Sprintf("INSERT INTO %s (f_name, l_name, email, password, role, active) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id", usersTable)
	row := r.db.QueryRow(query, user.FirstName, user.LastName, user.Email, user.Password, user.Role, user.Active)
	if err := row.Scan(&id); err != nil {
		return 0, err
	}
	return id, nil
}

func (r *AuthDB) GetUser(email, password string) (model.User, error) {
	var user model.User
	query := fmt.Sprintf("SELECT id FROM %s WHERE email=$1 AND password=$2", usersTable)
	err := r.db.Get(&user, query, email, password)
	return user, err
}

func (r *AuthDB) GetUserRole(id int) (string, error) {
	// err := r.db.Ping()
	// if err != nil {
	// 	return "", errors.New("Can't pind database")
	// }
	var role string
	query := fmt.Sprintf("SELECT role FROM %s WHERE id=$1", usersTable)
	err := r.db.Get(&role, query, id)
	return role, err
}

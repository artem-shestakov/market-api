package repository

import (
	"errors"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

type ProductDB struct {
	db *sqlx.DB
}

func NewProductDB(db *sqlx.DB) *ProductDB {
	return &ProductDB{
		db: db,
	}
}

func (r *ProductDB) CreateProduct(categoryID int, product *model.Product) (int, error) {
	var productID int
	var exists bool
	// Check category exists
	categoryQuery := fmt.Sprintf("SELECT EXISTS (SELECT title FROM %s WHERE id=$1)", categoryTable)
	row := r.db.QueryRow(categoryQuery, categoryID)
	if err := row.Scan(&exists); err != nil || !exists {
		return 0, errors.New(fmt.Sprintf("Can't found category ID: %d", categoryID))
	}
	// Create product
	query := fmt.Sprintf("INSERT INTO %s (category_id, title, description, price, currency) VALUES ($1, $2, $3, $4, $5) RETURNING id", productsTable)
	rowProduct := r.db.QueryRow(query, categoryID, product.Title, product.Description, product.Price, product.Currency)
	if err := rowProduct.Scan(&productID); err != nil {
		return 0, err
	}
	return productID, nil
}

func (r *ProductDB) GetAllProducts() ([]model.Product, error) {
	var products []model.Product
	query := fmt.Sprintf("SELECT * FROM %s", productsTable)
	err := r.db.Select(&products, query)
	return products, err
}

func (r *ProductDB) GetProduct(id int) (model.Product, error) {
	var product model.Product
	query := fmt.Sprintf("SELECT * FROM %s WHERE id=$1", productsTable)
	err := r.db.Get(&product, query, id)
	return product, err
}

func (r *ProductDB) DeleteProduct(id int) error {
	query := fmt.Sprintf("DELETE FROM %s WHERE id=$1", productsTable)
	res, err := r.db.Exec(query, id)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return errors.New(fmt.Sprintf("Can't found product ID: %d", id))
	}
	return nil
}

func (r *ProductDB) Update(id int, data *model.UpdateProduct) (*model.Product, error) {
	product := model.Product{}
	// Parse new values, create args for query and count args
	sets := make([]string, 0)
	args := make([]interface{}, 0)
	argID := 1

	if data.CategoryID != 0 {
		sets = append(sets, fmt.Sprintf("category_id=$%d", argID))
		args = append(args, data.CategoryID)
		argID++
	}
	if data.Title != "" {
		sets = append(sets, fmt.Sprintf("title=$%d", argID))
		args = append(args, data.Title)
		argID++
	}
	if data.Description != "" {
		sets = append(sets, fmt.Sprintf("description=$%d", argID))
		args = append(args, data.Description)
		argID++
	}
	if data.Price != 0 {
		sets = append(sets, fmt.Sprintf("price=$%d", argID))
		args = append(args, data.Price)
		argID++
	}
	if data.Currency != "" {
		sets = append(sets, fmt.Sprintf("currency=$%d", argID))
		args = append(args, data.Currency)
		argID++
	}

	setQuery := strings.Join(sets, ", ")
	query := fmt.Sprintf("UPDATE %s SET %s WHERE id=%d RETURNING *", productsTable, setQuery, id)
	if err := r.db.Get(&product, query, args...); err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(fmt.Sprintf("Can't found product ID: %d", id))
	}
	// rows := r.db.QueryRow(query, args...)
	// if err := rows.Scan(&product); err != nil {
	// 	fmt.Println(err.Error())
	// 	return errors.New(fmt.Sprintf("Can't found product ID: %d", id))
	// }
	// res, err := r.db.Exec(query, args...)
	// if err != nil {
	// 	return err
	// }
	// countRow, err := res.RowsAffected()
	// if err != nil {
	// 	return err
	// }
	// if countRow == 0 {
	// 	return errors.New(fmt.Sprintf("Can't found product ID: %d", id))
	// }
	return &product, nil
}

func (r *ProductDB) AddToCard(product_id int, cart_id int, quantity int) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	query := fmt.Sprintf("INSERT INTO %s (cart_id, product_id, quantity) VALUES ($1, $2, $3)", cartsProductsTable)
	_, err = tx.Exec(query, cart_id, product_id, quantity)
	if err != nil {
		tx.Rollback()
		return err
	}
	updateQuery := fmt.Sprintf("UPDATE %s SET updated_at=CURRENT_TIMESTAMP WHERE id=$1 RETURNING id", cartsTable)
	row := tx.QueryRow(updateQuery, cart_id)
	if err := row.Scan(&cart_id); err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

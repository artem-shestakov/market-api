package repository

import (
	"errors"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
)

type CartDB struct {
	db *sqlx.DB
}

func NewCardDB(db *sqlx.DB) *CartDB {
	return &CartDB{
		db: db,
	}
}

// Get new cart cart to add product
func (r *CartDB) GetNewCart(user_id int) (int, error) {
	var cart_id int
	query := fmt.Sprintf("SELECT id FROM %s WHERE status='new'", cartsTable)
	row := r.db.QueryRow(query)
	if err := row.Scan(&cart_id); err != nil {
		return 0, err
	}
	return cart_id, nil
}

// Create new cart
func (r *CartDB) CreateCart(user_id int) (int, error) {
	var cartID int
	// Check exists users's carts whith status new
	existID, err := r.GetNewCart(user_id)
	if existID == 0 && strings.Contains(err.Error(), "no rows in result set") {
		// Create cart
		query := fmt.Sprintf("INSERT INTO %s (user_id) VALUES ($1) RETURNING id", cartsTable)
		row := r.db.QueryRow(query, user_id)
		if err := row.Scan(&cartID); err != nil {
			return 0, err
		}
		return cartID, nil
	}
	// If cart already exists
	if existID != 0 {
		return 0, errors.New("Cart already exists for this user")
	}
	return 0, err
}

// Change cart status
func (r *CartDB) ChangeStatus(cart_id int, status string) error {
	query := fmt.Sprintf("UPDATE %s SET status=$1 WHERE id=%d", cartsTable, cart_id)
	res, err := r.db.Exec(query, status)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count < 0 {
		return errors.New(fmt.Sprintf("Can't update cart status. Cart ID: %d", cart_id))
	}
	return nil
}

func (r *CartDB) GetUserCart(user_id int) ([]model.CartRow, error) {
	var cartRows []model.CartRow
	cartID, err := r.GetNewCart(user_id)
	if err != nil {
		return nil, err
	}
	query := fmt.Sprintf("SELECT p.id, p.title, p.price, cp.quantity, p.price*cp.quantity as sum FROM %s p INNER JOIN %s cp ON p.id=cp.product_id WHERE cp.cart_id=$1", productsTable, cartsProductsTable)
	err = r.db.Select(&cartRows, query, cartID)
	return cartRows, err
}

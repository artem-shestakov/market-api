FROM golang:1.16-alpine
WORKDIR /go/src/app/
RUN  apk add --no-cache make gcc g++ curl && \
     curl -L https://github.com/golang-migrate/migrate/releases/download/v4.14.1/migrate.linux-amd64.tar.gz | tar xvz 
COPY . .
RUN make

FROM alpine:3
WORKDIR /app
COPY ./.env .
COPY --from=0 /go/src/app/migrate.linux-amd64 /usr/local/bin/migrate
COPY --from=0 /go/src/app/db ./db
COPY --from=0 /go/src/app/server .
EXPOSE 8000
ENTRYPOINT ["./server", "run"]

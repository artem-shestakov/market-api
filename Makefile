build:
	go build -v ./cmd/server.go

docs:
	apidoc -i internal/app/handler -o docs

.DEFAULT_GOAL := build

## Preparation
### Requirements
* NodeJS and `npm` for generating API docs
* Docker engine + Docker Compose

### .env file
Create .env file like this example:
```
DB_ADDRESS=db
DB_PORT=5432
DB_USER=director
DB_NAME=market
DB_PASSWORD=market
```
* DB_ADDRESS - Database address
* DB_PORT - Database port
* DB_USER - Database administrator user
* DB_NAME - Database name
* DB_PASSWORD - Database password

### Create API docs
Run `make docs` command

### Start app
Run containers by docker-compose
```bash
docker-compose up -d
```

### Database creating
To create database run this command:
```bash
docker-compose exec app /bin/sh -c 'migrate -database ${POSTGRESQL_URL} -path db/migrations up'
```
### Database migration
```bash
# Increase(update) version by <number>
docker-compose exec app /bin/sh -c 'migrate -database ${POSTGRESQL_URL} -path db/migrations up <number>'
# Decrease(rollback) version by <number>
docker-compose exec app /bin/sh -c 'migrate -database ${POSTGRESQL_URL} -path db/migrations down <number>' 
```
### Create user with `administrator` role
To create administarator run this command:
```bash
docker-compose exec app ./server createadmin --fname Artem --lname Shestakov
```
or
```bash
docker-compose exec app ./server createadmin -f Artem -l Shestakov
 ```
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/go-playground/validator"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/artem-shestakov/market-api/internal/app/handler"
	"gitlab.com/artem-shestakov/market-api/internal/app/model"
	"gitlab.com/artem-shestakov/market-api/internal/app/repository"
	"gitlab.com/artem-shestakov/market-api/internal/app/server"
	"gitlab.com/artem-shestakov/market-api/internal/app/service"
)

// Create administrator
func createAdmin(c *cli.Context) model.User {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Administrator email: ")
	email, _ := reader.ReadString('\n')
	fmt.Print("Administrator password: ")
	pass, _ := reader.ReadString('\n')
	admin := model.User{
		Email:     strings.TrimSpace(email),
		Password:  strings.TrimSpace(pass),
		FirstName: c.String("fname"),
		LastName:  c.String("lname"),
		Active:    true,
	}
	fmt.Println(admin)
	return admin
}

// getEnv getting env variables or set default value
func getEnv(lookEnv string, defEnv string) string {
	env, exist := os.LookupEnv(lookEnv)
	if !exist {
		env = defEnv
	}
	return env
}

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("Eroor with laoading .env file: %s", err.Error())
	}

	database, err := repository.NewDB(&repository.Database{
		Address:  getEnv("DB_ADDRESS", "localhost"),
		Port:     getEnv("DB_PORT", "5432"),
		User:     getEnv("DB_USER", "postgres"),
		DBName:   getEnv("DB_NAME", "postgres"),
		Password: getEnv("DB_PASSWORD", "postgres"),
		SSLMode:  getEnv("DB_SSL_MODE", "disable"),
	})
	if err != nil {
		logrus.Fatal("Database connection error")
	}

	validate := validator.New()

	repository := repository.NewRepository(database)
	service := service.NewService(repository)
	handler := handler.NewHandler(service, validate)
	// srv := server.NewServer()
	// srv.Run("0.0.0.0", "8000", handler.InitRouter())

	cmdApp := &cli.App{
		Name:  "Market-API",
		Usage: "Simple example market API",
		Commands: []*cli.Command{
			{
				Name:  "run",
				Usage: "Run API server",
				Action: func(c *cli.Context) error {
					srv := server.NewServer()
					srv.Run("0.0.0.0", "8000", handler.InitRouter())
					return nil
				},
			},
			{
				Name:  "createadmin",
				Usage: "Add market administrator",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "fname",
						Aliases:  []string{"f"},
						Usage:    "User's first name",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "lname",
						Aliases:  []string{"l"},
						Usage:    "User's last name",
						Required: false,
					},
				},
				Action: func(c *cli.Context) error {
					administrator := createAdmin(c)
					if err := validate.Struct(administrator); err != nil {
						logrus.Error("Can't create administrator. ", err.Error())
						return err
					}
					id, err := service.CreateUser(&administrator, "administrator")
					if err != nil {
						logrus.Error("Can't create administrator. ", err.Error())
						return err
					}
					logrus.Info("Created user with role 'administrator'. User's ID: ", id)
					return nil
				},
			},
		},
	}

	err = cmdApp.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}

CREATE TABLE IF NOT EXISTS carts
(
    id serial PRIMARY KEY,
    user_id int NOT NULL,
    status varchar(20) NOT NULL DEFAULT 'new',
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS carts_products
(
    cart_id int,
    product_id int,
    quantity int,
    FOREIGN KEY (cart_id) REFERENCES carts(id) ON UPDATE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products(id) ON UPDATE CASCADE
);
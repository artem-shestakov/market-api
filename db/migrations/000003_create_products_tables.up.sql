CREATE TABLE IF NOT EXISTS products
(
    id serial PRIMARY KEY,
    category_id int NOT NULL,
    title varchar(255) UNIQUE NOT NULL,
    description text,
    price numeric NOT NULL,
    FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE CASCADE
);
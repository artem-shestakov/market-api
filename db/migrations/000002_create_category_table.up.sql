CREATE TABLE IF NOT EXISTS category
(
    id serial,
    title VARCHAR(100) NOT NULL UNIQUE,
    description text,
    PRIMARY KEY (id)
);
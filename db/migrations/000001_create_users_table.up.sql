CREATE TABLE IF NOT EXISTS users
(
    id serial PRIMARY KEY,
    f_name varchar(255) NOT NULL,
    l_name varchar(255) NOT NULL,
    email varchar(255) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    role varchar(13),
    active boolean
);